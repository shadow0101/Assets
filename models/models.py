# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
from datetime import date, datetime, timedelta


class Asset(models.Model):
    _name = 'assets.asset'

    name = fields.Char(string="Asset name", required=True)
    code = fields.Char(string="Serial Number", required=True)
    category_id = fields.Many2one('assets.category', ondelete='cascade',
                                  string="Category", required=True)
    department_id = fields.Many2one('assets.department', string="Department", ondelete='cascade', required=False)
    date = fields.Date(string="Acquisition Date", default=fields.Date.today)
    cost = fields.Integer(string="Acquisition Cost", required=True)
    salvage = fields.Integer(string="Salvage Value", required=True)
    location_id = fields.Many2one('assets.location', ondelete='cascade',
                                  string="Location", required=False)
    supplier_id = fields.Many2one('assets.supplier', required=False, string="Supplier")
    depreciation_method = fields.Selection(
        [('line', "Straight Line"), ('double', "Double Declining"), ],
        string="Computation Method", default="line")
    depreciation_life = fields.Integer(string="Asset's Lifespan", default=1)
    state = fields.Selection([('draft', "Draft"), ('confirm', "Confirm"), ('active', "Active"), ], default='draft',
                             string="Status")
    depreciation_line = fields.One2many('assets.depreciation', 'asset_id', string="Depreciation Lines")
    depreciable_cost = fields.Char(string="Depreciable Cost", readonly=True)
    depreciation_year = fields.Float(string="Depreciation/Year", readonly=True)
    custodian_id = fields.Many2one('assets.custodian', string="Custodian")

    @api.multi
    def action_draft(self):
        self.state = 'draft'
        for line in self.depreciation_line:
            line.unlink()
        self.depreciable_cost=''
        self.depreciation_year=0
    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        self.ensure_one()
        lists = []
        if self.depreciation_method == 'line':
            amount = (self.cost - self.salvage)
            rate = (100 / self.depreciation_life) * 0.01
            diem = datetime.strptime(self.date, '%Y-%m-%d')
            self.depreciable_cost = "PHP " + str(amount)
            self.depreciation_year = amount/self.depreciation_life
            annual = amount * rate
            for x in range(self.depreciation_life):
                values = {
                    'depreciation_value': amount,
                    'annual_depreciation': annual,
                    'depreciation_rate': rate,
                    'depreciation_date': diem,
                }
                amount -= annual
                diem = diem + timedelta(days=365)
                lists.append((0, False, values))

            self.write({'depreciation_line': lists})
        elif self.depreciation_method == 'double':
            rate = (100 / self.depreciation_life) * 0.02
            amount = self.cost
            diem = datetime.strptime(self.date, '%Y-%m-%d')
            while amount > self.salvage:
                annual = amount * rate
                values = {
                    'depreciation_value': amount,
                    'annual_depreciation': annual,
                    'depreciation_rate': rate,
                    'depreciation_date': diem
                }
                diem = diem + timedelta(days=365)
                amount -= annual

                lists.append((0, False, values))
            self.write({'depreciation_line': lists})
            self.depreciable_cost = "PHP " + str(amount + annual)
        return amount

    @api.multi
    def action_active(self):
        self.state = 'active'


class Depreciation(models.Model):
    _name = 'assets.depreciation'
    name = fields.Char(string='Depreciation name')
    asset_id = fields.Many2one('assets.asset', string="asset id", ondelete='cascade')
    depreciation_value = fields.Float(string="Depreciable Value")
    annual_depreciation = fields.Float(string="Annual Depreciation")
    depreciation_rate = fields.Float(string="Rate")
    depreciation_date = fields.Date('Date')


class Category(models.Model):
    _name = 'assets.category'
    name = fields.Char(string="Type", required=False)


class Location(models.Model):
    _name = 'assets.location'
    name = fields.Char(required=True)
    address = fields.Char(required=True)


class Supplier(models.Model):
    _name = 'assets.supplier'
    name = fields.Char(string="Supplier name", required=True)
    phone = fields.Char(required=False)
    address = fields.Char(required=False)


class Department(models.Model):
    _name = 'assets.department'
    name = fields.Char(string="Department", required=True)


class Custodian(models.Model):
    _name='assets.custodian'
    name= fields.Char(string="Custodian", required=True)