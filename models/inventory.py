# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class Inventory(models.Model):
    _name = 'assets.inventory'
    _inherit='assets.asset'
